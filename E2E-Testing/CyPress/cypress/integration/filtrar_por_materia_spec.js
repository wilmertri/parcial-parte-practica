/**
 * Created by wilme on 23/09/2018.
 */
describe('Filtro por materia', function() {
    it('Filtro por materia en página de profesor', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        //Lineas nuevas
        cy.get('.buscador').find('input').type('Fernando Arruza Hedman', { force: true})
        cy.contains('Fernando Arruza Hedman - Ingeniería de Sistemas').click()
        cy.wait(2000)
        cy.get('.materias').find('input[name="id:ISIS4426"]').click()
    })
})