/**
 * Created by wilme on 23/09/2018.
 */
describe('Los estudiantes login failed', function() {
    it('Visits los estudiantes and login failed', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        //Login correcto
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("mailerror@mail.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("142536758")
        cy.get('.cajaLogIn').contains('Ingresar').click()
        cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    })
})