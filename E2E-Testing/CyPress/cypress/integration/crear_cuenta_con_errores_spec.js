describe('Create account whit errors', function() {
    it('Create account whit errors', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        //Lineas nuevas
        cy.contains('Ingresar').click()
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Antonio")
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Ledesma")
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("antoniolede1234gmail.com")
        cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Ingeniería Biomédica')
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("123456789")
        cy.get('.cajaSignUp').contains('Registrarse').click()
        cy.contains("Debes aceptar los términos y condiciones")
        cy.contains("Ingresa un correo valido")
    })
})