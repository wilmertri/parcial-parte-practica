/**
 * Created by wilme on 24/09/2018.
 */
describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomEvent(20);
    })
})

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};

function randomEvent(monkeysLeft) {

    var event = getRandomInt(1,5);
    console.log(event);

    if(event == 1)
    {
        randomClick(monkeysLeft);
    }
    if(event == 2)
    {
        randomFillInput(monkeysLeft);
    }
    if(event == 3)
    {
        randomSelectedCombo(monkeysLeft);
    }
    if(event == 4)
    {
        randomClickButton(monkeysLeft);
    }

}

function randomClick(monkeysLeft) {

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0)
    {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if(!Cypress.dom.isHidden(randomLink)) {
                cy.wrap(randomLink).click({force: true});
                monkeysLeft = monkeysLeft - 1;
            }
            setTimeout(randomClick, 1000, monkeysLeft);
        });

        return monkeysLeft * randomClick(monkeysLeft - 1);
    }


}

function randomFillInput(monkeysLeft) {

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0)
    {
        cy.get('input').then($inputs => {
            var randomInput = $inputs.get(getRandomInt(0, $inputs.length));
            cy.wrap(randomInput).type('Fernando Arruza',{force: true});
            cy.wrap(randomInput).clear();
            monkeysLeft = monkeysLeft - 1;
            setTimeout(randomFillInput, 1000, monkeysLeft);
        });

        return monkeysLeft * randomFillInput(monkeysLeft - 1);
    }
}

function randomSelectedCombo(monkeysLeft) {
    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0)
    {
        cy.get('.buscador').find('input').type('Fernando Arruza Hedman', { force: true});
        cy.contains('Fernando Arruza Hedman - Ingeniería de Sistemas').click();
        cy.get('input[type="checkbox"]').then($inputs => {
            var randomInput = $inputs.get(getRandomInt(0, $inputs.length));
            cy.wrap(randomInput).click({force: true});
            monkeysLeft = monkeysLeft - 1;
            setTimeout(randomSelectedCombo, 1000, monkeysLeft);
        });
        cy.get('.buscador').clear();
        return monkeysLeft * randomSelectedCombo(monkeysLeft - 1);
    }
}

function randomClickButton(monkeysLeft) {
    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0)
    {

        cy.get('button').then($buttons => {
            var randomButton = $buttons.get(getRandomInt(0, $buttons.length));
            cy.wrap(randomButton).click({force: true});
            monkeysLeft = monkeysLeft - 1;
            setTimeout(randomClickButton, 1000, monkeysLeft);
        });

        return monkeysLeft * randomClickButton(monkeysLeft - 1);
    }
}
